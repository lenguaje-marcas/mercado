package Herencias;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProductoRefrigerado extends Producto{
	private String CodigoSupervision;
	
	public ProductoRefrigerado(Date fechaCaducidad, int numero_lote, String CodigoSupervision) {
		super(fechaCaducidad, numero_lote);
		this.setCodigoSupervision(CodigoSupervision);
	}
	
	public String getCodigoSupervision() {
		return CodigoSupervision;
	}
	
	public void setCodigoSupervision(String CodigoSupervision) {
		this.CodigoSupervision = CodigoSupervision;
	}
	
	public String toString() {
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd-MM-yyyy");		
		String info = "-- Producto refrigerado --\nFecha caducidad: " + DateFormat.format(getCaducidad()) + "\nNúmero lote: " +
		getNumero_lote() + "\nCodigo supervisón: " + getCodigoSupervision() + "\n";
		
		return info;
	}
}