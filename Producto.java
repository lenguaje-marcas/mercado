package Herencias;
import java.util.Date;

public class Producto{
	private Date fechaCaducidad;
	private int numero_lote;
	
	public Producto(Date fechaCaducidad, int numero_lote){
		this.fechaCaducidad = fechaCaducidad;
		this.numero_lote = numero_lote;
	}
	
	public Date getCaducidad() {
		return fechaCaducidad;
	}
	
	public void setfechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;	
	}
	
	public int getNumero_lote() {
		return numero_lote;
	}
	
	public void setNumero_lote(int numero_lote) {
		this.numero_lote = numero_lote;
	}
	
}