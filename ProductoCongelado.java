package Herencias;
import java.util.Date;
import java.text.SimpleDateFormat;

public class ProductoCongelado extends Producto{
	private int TemperaturaRecomendada;
	
	public ProductoCongelado(Date fechaCaducidad, int numero_lote, int TemperaturaRecomendada) {
		super(fechaCaducidad, numero_lote);
		this.setTemperaturaRecomendada(TemperaturaRecomendada);
	}
	
	public int getTemperaturaRecomendada() {
		return TemperaturaRecomendada;
	}
	
	public void setTemperaturaRecomendada(int TemperaturaRecomendada) {
		this.TemperaturaRecomendada = TemperaturaRecomendada;
	}
	
	public String toString() {
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd-MM-yyy");
		String info = "-- Producto congelado --\nFecha caducidad:" + DateFormat.format(getCaducidad()) + "\nNumero lote: " + getNumero_lote() +
				"\nTemperatura recomendada: " + getTemperaturaRecomendada() + "º\n";
		return info;
	}
}