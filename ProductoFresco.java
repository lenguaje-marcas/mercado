package Herencias;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProductoFresco extends Producto{
	private Date fechaEnvasado;
	private String PaisOrigen;

	public ProductoFresco(Date fechaCaducidad, int numeroLote, Date fechaEnvasado, String PaisOrigen) {
		super(fechaCaducidad, numeroLote);
		this.setFechaEnvasado(fechaEnvasado);
		this.setPaisOrigen(PaisOrigen);
	}
	
	public Date getFechaEnvasado() {
		return fechaEnvasado;
	}
	
	public void setFechaEnvasado(Date fechaEnvasado) {
		this.fechaEnvasado = fechaEnvasado;
	}
	public String getPaisOrigen() {
		return PaisOrigen;
	}
	
	public void setPaisOrigen(String PaisOrigen) {
		this.PaisOrigen = PaisOrigen;	
	}
	
	
	public String toString() {
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String info = "-- Producto fresco --\nFecha caducidad: " + DateFormat.format(getCaducidad()) +
				"\nNumero lote: " + getNumero_lote() + "\nFecha envasado: " +
				DateFormat.format(getFechaEnvasado())+
				"\nPais origen: " + getPaisOrigen() + "\n";
		return info;
	}
}
	
	
