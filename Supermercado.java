package Herencias;
import java.util.Scanner;
import java.util.Date;

public class Supermercado{
	public void MostrarMenu() {
		Scanner leer = new Scanner(System.in);
		int opcion;
		do {
			System.out.println("---- Supermercado ----");
			System.out.println("1. Producto fresco");
			System.out.println("2. Producto refrigerado");
			System.out.println("3. Producto congelado");
			System.out.println("Selecciona una opción: ");
			
			opcion = leer.nextInt();
			leer.nextLine();
			
			switch (opcion) {
			case 1:
				nuevoProductoFresco(leer);
				break;
			case 2:
				nuevoProductoRefrigerado(leer);
			case 3:
				nuevoProductoCongelado(leer);
			}
		} while (opcion != 0);
		leer.close();
		
	}
	
	private void nuevoProductoFresco(Scanner leer) {
		String nombre;
		String fechaCaducidad;
		int numeroLote;
		String fechaEnvasado;
		String PaisOrigen;
		
		System.out.println("Introduce nombre del producto fresco: ");
		nombre = leer.nextLine();
		System.out.println("Introduce la fecha de caducidad (dd-mm-yyyy): ");
		fechaCaducidad = leer.nextLine();
		
		System.out.println("Introduce el número de lote: ");
		numeroLote = leer.nextInt();
		
		System.out.println("Introduce la fecha de envasado: ");
		fechaEnvasado = leer.nextLine();
		
		System.out.println("Introduce el pais de origen: ");
		PaisOrigen = leer.nextLine();
		
		ProductoFresco producto1 = new ProductoFresco(fechaCaducidad, numeroLote, fechaEnvasado, PaisOrigen);
		
	}
	
	private void nuevoProductoRefrigerado(Scanner leer) {
		
	}
	
	private void nuevoProductoCongelado(Scanner leer) {
		
	}
	
	
	
	
	
	public static void main(String[] args) {
		Date fechaCaducidad = new Date();
		int numeroLote = 1;
		Date fechaEnvasado = new Date();
		String PaisOrigen = "España";
		ProductoFresco queso = new ProductoFresco(fechaCaducidad, numeroLote, fechaEnvasado, PaisOrigen);
		
		Date fechaCaducidad2 = new Date();
		int numeroLote2 = 123;
		String CodigoSupervision = "A33";
		ProductoRefrigerado leche = new ProductoRefrigerado(fechaCaducidad2, numeroLote2, CodigoSupervision);
		
		Date fechaCaducidad3 = new Date();
		int numeroLote3 = 321;
		int temperaturaRecomendada = -1;
		ProductoCongelado hielo = new ProductoCongelado(fechaCaducidad3, numeroLote3, temperaturaRecomendada);
		
		System.out.println(queso.toString());
		System.out.println(leche.toString());
		System.out.println(hielo.toString());
		
		
		//pruebasasd
		ProductoCongelado salmon = new ProductoCongelado(new Date(), 12, -5);
		System.out.println(salmon.toString());
		
	}
	
	 	
}